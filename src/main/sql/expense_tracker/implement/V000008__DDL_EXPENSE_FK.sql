ALTER TABLE expense_tracker.expense
    ADD CONSTRAINT expense_category_fk FOREIGN KEY ( category_id )
        REFERENCES expense_tracker.category ( id );


ALTER TABLE expense_tracker.expense
    ADD CONSTRAINT expense_user_owner_fk FOREIGN KEY ( user_id )
        REFERENCES expense_tracker.`USER` ( id );


CREATE TRIGGER ins_expense BEFORE INSERT ON expense_tracker.`expense` for each row
          SET NEW.created_date = NOW();

CREATE TRIGGER upd_expense BEFORE UPDATE ON expense_tracker.`expense` for each row
          SET NEW.last_updated_date = NOW();