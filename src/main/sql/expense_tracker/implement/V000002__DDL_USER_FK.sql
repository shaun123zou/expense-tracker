ALTER TABLE expense_tracker.`USER`
    ADD CONSTRAINT user_user_created_fk FOREIGN KEY ( created_by )
        REFERENCES expense_tracker.`USER` ( id );

ALTER TABLE expense_tracker.`USER`
    ADD CONSTRAINT user_user_last_upd_fk FOREIGN KEY ( last_updated_by )
        REFERENCES expense_tracker.`USER` ( id );

CREATE TRIGGER ins_user BEFORE INSERT ON expense_tracker.`USER` for each row
          SET NEW.created_date = NOW();


CREATE TRIGGER upd_user BEFORE UPDATE ON expense_tracker.`USER` for each row
          SET NEW.last_updated_date = NOW();

