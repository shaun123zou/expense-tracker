CREATE TABLE expense_tracker.`category` (
    id                  INTEGER NOT NULL AUTO_INCREMENT,
    name                VARCHAR(100) NOT NULL,
    description         VARCHAR(250),
    expense_type_id     INTEGER NOT NULL,
    status              CHAR(1) NOT NULL,
    created_date        DATETIME NOT NULL,
    created_by          INTEGER NOT NULL,
    last_updated_date   DATETIME,
    last_updated_by     INTEGER,
    PRIMARY KEY (id)
);

