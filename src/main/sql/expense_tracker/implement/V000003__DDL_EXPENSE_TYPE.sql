CREATE TABLE expense_tracker.`expense_type` (
    id                  INTEGER NOT NULL AUTO_INCREMENT,
    name                VARCHAR(100) NOT NULL,
    description         VARCHAR(250),
    pre_defined         CHAR(1) NOT NULL,
    for_user            INTEGER NOT NULL,
    status              CHAR(1) NOT NULL,
    created_date        DATETIME NOT NULL,
    created_by          INTEGER NOT NULL,
    last_updated_date   DATETIME,
    last_updated_by     INTEGER,
    PRIMARY KEY (id)
);

