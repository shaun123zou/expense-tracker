ALTER TABLE expense_tracker.expense_type
    ADD CONSTRAINT expense_type_user_created_fk FOREIGN KEY ( created_by )
        REFERENCES expense_tracker.`USER` ( id );

ALTER TABLE expense_tracker.expense_type
    ADD CONSTRAINT expense_type_user_last_upd_fk FOREIGN KEY ( last_updated_by )
        REFERENCES expense_tracker.`USER` ( id );

ALTER TABLE expense_tracker.expense_type
    ADD CONSTRAINT expense_type_user_owner_fk FOREIGN KEY ( for_user )
        REFERENCES expense_tracker.`USER` ( id );

CREATE TRIGGER ins_expense_type BEFORE INSERT ON expense_tracker.`expense_type` for each row
          SET NEW.created_date = NOW();


CREATE TRIGGER upd_expense_type BEFORE UPDATE ON expense_tracker.`expense_type` for each row
          SET NEW.last_updated_date = NOW();