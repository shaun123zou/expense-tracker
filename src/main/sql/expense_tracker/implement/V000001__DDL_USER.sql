CREATE TABLE expense_tracker.`USER` (
    id                  INTEGER NOT NULL AUTO_INCREMENT,
    first_name          VARCHAR(100) NOT NULL,
    last_name           VARCHAR(100),
    email_address       VARCHAR(200) NOT NULL,
    date_of_birth       DATETIME,
    status              CHAR(1) NOT NULL,
    created_date        DATETIME NOT NULL,
    created_by          INTEGER,
    last_updated_date   DATETIME,
    last_updated_by     INTEGER,
    PRIMARY KEY (id)
);


