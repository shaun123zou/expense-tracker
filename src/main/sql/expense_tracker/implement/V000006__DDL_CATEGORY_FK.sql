ALTER TABLE expense_tracker.category
    ADD CONSTRAINT category_expense_type_fk FOREIGN KEY ( expense_type_id )
        REFERENCES expense_tracker.expense_type ( id );

ALTER TABLE expense_tracker.category
    ADD CONSTRAINT category_user_created_fk FOREIGN KEY ( created_by )
        REFERENCES expense_tracker.`USER` ( id );

ALTER TABLE expense_tracker.category
    ADD CONSTRAINT category_user_last_upd_fk FOREIGN KEY ( last_updated_by )
        REFERENCES expense_tracker.`USER` ( id );

CREATE TRIGGER ins_category BEFORE INSERT ON expense_tracker.`category` for each row
          SET NEW.created_date = NOW();

CREATE TRIGGER upd_category BEFORE UPDATE ON expense_tracker.`category` for each row
          SET NEW.last_updated_date = NOW();