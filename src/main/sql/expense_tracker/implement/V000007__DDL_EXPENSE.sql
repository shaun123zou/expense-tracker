CREATE TABLE expense_tracker.`expense` (
    id                  INTEGER NOT NULL AUTO_INCREMENT,
    user_id             INTEGER NOT NULL,
    category_id         INTEGER NOT NULL,
    `date`              DATETIME NOT NULL,
    amount              NUMERIC(12, 2) NOT NULL,
    note                VARCHAR(250),
    created_date        DATETIME NOT NULL,
    last_updated_date   DATETIME,
    PRIMARY KEY (id)
);