package xyz.shanmugavel.expensetracker.service;

import java.util.List;
import xyz.shanmugavel.expensetracker.persistence.model.Category;

public interface ICategoryService {
  List<Category> fetchAll();
}
