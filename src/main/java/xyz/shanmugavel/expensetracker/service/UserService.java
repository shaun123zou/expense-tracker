package xyz.shanmugavel.expensetracker.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import xyz.shanmugavel.expensetracker.persistence.model.User;
import xyz.shanmugavel.expensetracker.persistence.repository.UserRepository;

@Service
@Slf4j
public class UserService implements IUserService {

  @Autowired
  private UserRepository repository;

  @Override
  public List<User> fetchAll() {
    return repository.findAll();
  }

  @Override
  public Optional<User> findById(Integer id) {
    return repository.findById(id);
  }

  @Override
  public User save(User user) {
    User usr = repository.save(user);
    log.info("User Object with Id [{}] Saved", usr.getId());
    return usr;
  }

  @Override
  public boolean delete(Integer id) {
    repository.deleteById(id);
    return true;
  }
}
