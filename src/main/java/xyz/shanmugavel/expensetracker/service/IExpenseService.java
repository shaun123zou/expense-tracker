package xyz.shanmugavel.expensetracker.service;

import java.time.LocalDate;
import java.util.List;
import xyz.shanmugavel.expensetracker.persistence.model.Expense;

public interface IExpenseService {

  List<Expense> findByUserAndDate(Integer userId, LocalDate date);
}
