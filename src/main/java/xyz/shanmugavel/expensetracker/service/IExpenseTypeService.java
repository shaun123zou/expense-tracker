package xyz.shanmugavel.expensetracker.service;

import java.util.List;
import xyz.shanmugavel.expensetracker.persistence.model.ExpenseType;

public interface IExpenseTypeService {
  List<ExpenseType> fetchAll();
}
