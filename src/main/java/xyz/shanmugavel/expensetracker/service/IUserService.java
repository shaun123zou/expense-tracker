package xyz.shanmugavel.expensetracker.service;

import java.util.List;
import java.util.Optional;
import xyz.shanmugavel.expensetracker.persistence.model.User;

public interface IUserService {

    List<User> fetchAll();
    Optional<User> findById(Integer id);
    User save(User user);
    boolean delete(Integer id);

}
