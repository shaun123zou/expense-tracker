package xyz.shanmugavel.expensetracker.controller;

import java.util.List;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.shanmugavel.expensetracker.persistence.model.User;
import xyz.shanmugavel.expensetracker.service.IUserService;

@RestController
@Slf4j
@RequestMapping("/api")
public class UserController {

  @Autowired
  private IUserService userService;

  @GetMapping(path = "users", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<User>> getAllUsers() {
    List<User> users = userService.fetchAll();
    ResponseEntity<List<User>> resp;
    if (users != null && !users.isEmpty()) {
      resp = ResponseEntity.of(Optional.of(users));
    } else {
      resp = ResponseEntity.noContent().build();
      log.info("No users are found!");
    }
    return resp;
  }
}
