package xyz.shanmugavel.expensetracker.persistence.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class Expense implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @ManyToOne
  @JoinColumn(name = "USER_ID")
  private User user;
  @ManyToOne
  @JoinColumn(name = "CATEGORY_ID")
  private Category category;
  private LocalDate date;
  private BigDecimal amount;
  private String note;
  private LocalDate createdDate;
  private LocalDate lastUpdatedDate;
}
