package xyz.shanmugavel.expensetracker.persistence.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString(exclude = {"createdBy", "lastUpdatedBy"})
@EqualsAndHashCode
public class Category implements Serializable {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  private String name;
  private String description;
  private Character status;
  private LocalDate createdDate;
  private LocalDate lastUpdatedDate;
  @OneToOne()
  @JoinColumn(name = "CREATED_BY")
  private User createdBy;
  @OneToOne
  @JoinColumn(name = "LAST_UPDATED_BY")
  private User lastUpdatedBy;
}
