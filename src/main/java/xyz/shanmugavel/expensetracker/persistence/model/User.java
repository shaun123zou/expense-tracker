package xyz.shanmugavel.expensetracker.persistence.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString(exclude = {"createdBy", "lastUpdatedBy"})
@EqualsAndHashCode
public class User implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column(name = "FIRST_NAME")
  private String firstName;
  @Column(name = "LAST_NAME")
  private String lastName;
  @Column(name = "EMAIL_ADDRESS")
  private String email;
  @Column(name = "DATE_OF_BIRTH")
  private LocalDate dateOfBirth;
  @Column(name = "STATUS")
  private Character status;
  @Column(name = "CREATED_DATE")
  private LocalDate createdDate;
  @Column(name = "LAST_UPDATED_DATE")
  private LocalDate lastUpdatedDate;
  @OneToOne()
  @JoinColumn(name = "CREATED_BY")
  private User createdBy;
  @OneToOne
  @JoinColumn(name = "LAST_UPDATED_BY")
  private User lastUpdatedBy;

}
