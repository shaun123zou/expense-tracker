package xyz.shanmugavel.expensetracker.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.shanmugavel.expensetracker.persistence.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
