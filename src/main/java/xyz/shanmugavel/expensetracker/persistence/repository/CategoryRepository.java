package xyz.shanmugavel.expensetracker.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.shanmugavel.expensetracker.persistence.model.Category;
import xyz.shanmugavel.expensetracker.persistence.model.ExpenseType;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
