package xyz.shanmugavel.expensetracker.persistence.repository;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.shanmugavel.expensetracker.persistence.model.Category;
import xyz.shanmugavel.expensetracker.persistence.model.Expense;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Integer> {
  List<Expense> findByUserAndDate(Integer userId, LocalDate date);

}
